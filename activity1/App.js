import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View } from 'react-native';
import { Button } from 'react-native-web';

function first() {
  var str = document.getElementById("input-text").value;
  alert("You just type:  " + str)
}
export default function App() {
  return (
    <View style={styles.container}>
      
      <input id="input-text" type="input-text"></input>
      <button onClick={first}>Button</button>
    </View>

  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
