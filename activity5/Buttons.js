import React, { Component } from 'react';
import { View, StyleSheet, Text, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

const myIcon = <Icon name="backspace-outline" size={30} color="#900" />;
const buttonRippel = {
  color: 'grey',
  borderless: true,
};

class Buttons extends Component {
  render() {
    const {
      getButtonPressedVal,
      allClear,
      del,
      showHistory,
      onEqualPress,
    } = this.props;
    return (
      <View style={styles.button}>
        <View style={styles.leftSideButtons}>
          <TouchableOpacity
            style={styles.leftSideButton}
            android_ripple={buttonRippel}
            onPress={() => {
              showHistory();
            }}>
            <Text style={styles.buttonText}>H</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.leftSideButton}
            android_ripple={buttonRippel}
            onPress={() => {
              allClear();
            }}>
            <Text style={{ fontSize: 20, fontWeight: 'bold', color: 'orangered' }}>AC</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.leftSideButton}
            android_ripple={buttonRippel}
            onPress={() => {
              getButtonPressedVal('%');
            }}>
            <Text style={styles.buttonText}>%</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.leftSideButton}
            android_ripple={buttonRippel}
            onPress={() => {
              getButtonPressedVal('7');
            }}>
            <Text style={styles.buttonText}>7</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.leftSideButton}
            android_ripple={buttonRippel}
            onPress={() => {
              getButtonPressedVal('8');
            }}>
            <Text style={styles.buttonText}>8</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.leftSideButton}
            android_ripple={buttonRippel}
            onPress={() => {
              getButtonPressedVal('9');
            }}>
            <Text style={styles.buttonText}>9</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.leftSideButton}
            android_ripple={buttonRippel}
            onPress={() => {
              getButtonPressedVal('4');
            }}>
            <Text style={styles.buttonText}>4</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.leftSideButton}
            android_ripple={buttonRippel}
            onPress={() => {
              getButtonPressedVal('5');
            }}>
            <Text style={styles.buttonText}>5</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.leftSideButton}
            android_ripple={buttonRippel}
            onPress={() => {
              getButtonPressedVal('6');
            }}>
            <Text style={styles.buttonText}>6</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.leftSideButton}
            android_ripple={buttonRippel}
            onPress={() => {
              getButtonPressedVal('1');
            }}>
            <Text style={styles.buttonText}>1</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.leftSideButton}
            android_ripple={buttonRippel}
            onPress={() => {
              getButtonPressedVal('2');
            }}>
            <Text style={styles.buttonText}>2</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.leftSideButton}
            android_ripple={buttonRippel}
            onPress={() => {
              getButtonPressedVal('3');
            }}>
            <Text style={styles.buttonText}>3</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.leftSideButton}
            android_ripple={buttonRippel}
            onPress={() => {
              getButtonPressedVal('0');
            }}>
            <Text style={styles.buttonText}>0</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.leftSideButton}
            android_ripple={buttonRippel}
            onPress={() => {
              getButtonPressedVal('.');
            }}>
            <Text style={styles.buttonText}>.</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.leftSideButton}
            android_ripple={buttonRippel}
            onPress={() => {
              del();
            }}>
            <Text style={styles.buttonText}>{myIcon}</Text>
          </TouchableOpacity>
        </View>

        <View style={styles.rightSideButtons}>
          <TouchableOpacity
            style={styles.rightSideButton}
            android_ripple={buttonRippel}
            onPress={() => {
              getButtonPressedVal('÷');
            }}>
            <Text style={styles.rightButtonText}>÷</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.rightSideButton}
            android_ripple={buttonRippel}
            onPress={() => {
              getButtonPressedVal('×');
            }}>
            <Text style={styles.rightButtonText}>×</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.rightSideButton}
            android_ripple={buttonRippel}
            onPress={() => {
              getButtonPressedVal('−');
            }}>
            <Text style={styles.rightButtonText}>−</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.rightSideButton}
            android_ripple={buttonRippel}
            onPress={() => {
              getButtonPressedVal('+');
            }}>
            <Text style={[styles.rightButtonText]}>+</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.rightSideButton}
            android_ripple={buttonRippel}
            onPress={() => {
              onEqualPress()
            }}>
            <Text
              style={{
                fontSize: 25,
                fontWeight: 'bold',
                backgroundColor: 'blue',
                padding: 15,
                borderRadius: 40,
                color: 'white'
              }}>
              =
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  button: {
    flex: 7,
    flexDirection: 'row',
    backgroundColor: 'white'
  },
  rightSideButtons: {
    flex: 2,
  },
  leftSideButtons: {
    flex: 8,
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  leftSideButton: {
    width: '33.33%',
    height: '20%',
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'black'
  },
  rightSideButton: {
    height: '20%',
    width: '80%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  rightButtonText: {
    fontSize: 35,
    fontWeight: 'bold',
    padding: 10,
    borderRadius: 40,
    color: 'blue'
  },
});

export default Buttons;
