import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity, ScrollView } from 'react-native';

import * as Style from '../assets/styles';
import Icon from 'react-native-vector-icons/Ionicons';
import AsyncStorage from '@react-native-async-storage/async-storage';

const add = <Icon name="add" size={20} color="black" />;


const Notes = ({ navigation, ...props }) => {


  function saveNote(index) {
    let newArray = [...props.notes];
    let saved = newArray.splice(index, 1);
    props.setNotes(newArray);
    props.setSavedNotes(saved);

    let bin = [saved, ...props.savedNotes];
    props.setSavedNotes(bin);

    AsyncStorage.setItem('savedNotes', JSON.stringify(bin)).then(() => {
      props.setSavedNotes(bin);
    }).catch(error => console.log(error));
  }

  return (
    <View style={[styles.notesContainer]}>
      <View style={[styles.headingContainer]}>
        <Text style={[styles.heading]}>Your Notes...</Text>
        <View style={{ flexDirection: 'row' }}>
          {/* Buttons */}
          <TouchableOpacity style={[styles.savebutton]}
            onPress={() => navigation.navigate('SavedNotes')}>
            <Text>Saved Notes</Text>
          </TouchableOpacity>

          <TouchableOpacity style={[styles.button]}
            onPress={() => navigation.navigate('AddNote')}>{add}</TouchableOpacity>
        </View>
      </View>

      <ScrollView style={styles.scrollView} showsVerticalScrollIndicator={false}>
        {
          props.notes.length === 0
            ?

            <View style={styles.emptyNoteContainer}>
              <Text style={styles.emptyNoteText}>Click + to add note!</Text>
            </View>
            :

            props.notes.map((item, index) =>
              <View style={styles.item} key={index}>
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                  <View style={styles.note}>
                    <Text style={styles.index}>{index + 1}. </Text>
                    <Text style={styles.text}>{item}</Text>
                  </View>

                  <TouchableOpacity >
                    <Text style={styles.delete} onPress={() => saveNote(index)}>Save</Text>
                  </TouchableOpacity>

                </View>
              </View>
            )}

      </ScrollView>
    </View>
  )
}
export const styles = StyleSheet.create({
  notesContainer: {
    paddingTop: 10,
    paddingHorizontal: 20,
    marginBottom: 70,
    opacity: 0.9
  },
  heading: {
    fontSize: 30,
    fontWeight: '700'
  },
  item: {
    marginBottom: 20,
    padding: 15,
    color: 'black',
    opacity: 0.8,
    marginTop: 10,
    shadowColor: Style.color,
    shadowOpacity: 0.5,
    shadowOffSet: { width: 0, height: 4 },
    shadowRadius: 8,
    elavation: 5,
    backgroundColor: 'white',
    borderWidth: 2,
    borderRadius: 5,
    borderLeftWidth: 15
  },
  index: {
    fontSize: 20,
    fontWeight: '800'
  },
  headingContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  button: {
    backgroundColor: Style.color,
    width: 35,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 10,
    height: 35
  },
  buttonText: {
    color: 'white',
    fontSize: 32,
    fontWeight: 800
  },
  scrollView: {
    marginBottom: 70
  },
  note: {
    flexDirection: 'row',
    width: '75%'
  },
  text: {
    fontWeight: '700',
    fontSize: 17,
    alignSelf: 'center'
  },
  delete: {
    color: 'black',
    fontWeight: '700',
    fontSize: 15
  },
  input: {
    height: 40,
    paddingHorizontal: 20,
    width: '65%',
    fontSize: 19,
    color: 'black',
    fontWeight: '600',
    opacity: 0.8,
    shadowColor: Style.color,
    shadowOpacity: 0.4,
    shadowRadius: 8,
    backgroundColor: 'white',
    borderColor: 'black',
    borderWidth: 2,
    borderRadius: 5
  },
  emptyNoteContainer: {
    alignItems: 'center',
    marginTop: 240
  },
  emptyNoteText: {
    color: 'gray',
    fontWeight: '600',
    fontSize: 15
  },
  savebutton: {
    backgroundColor: Style.color,
    width: 100,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 5,
    height: 40
  },
  deleteContainer: {
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20
  },

  dateContainer: {
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20
  },
});
export default Notes;
