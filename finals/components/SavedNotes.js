import React from 'react';
import { Text, ScrollView, View } from 'react-native';
import { styles } from './Notes'



const SavedNotes = ({ ...props }) => {

  return (
    <ScrollView>
      <View style={[styles.notesContainer]}>

        {props.savedNotes.length === 0
          ?
          <View style={styles.emptyNoteContainer}>
            <Text style={styles.emptyNoteText}>Nothing to show yet...</Text>
          </View>
          :
          props.savedNotes.map((item, index) =>

            <View style={styles.item} key={index}>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>

                <View style={styles.note}>
                  <Text style={styles.index}>{index + 1}. </Text>
                  <Text style={styles.text}>{item}</Text>
                </View>
              </View>

            </View>
          )
        }
      </View>

    </ScrollView >
  )
}
export default SavedNotes;
