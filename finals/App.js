import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Notes from './components/Notes';
import AddNote from './components/AddNote';

import React, { useState, useEffect } from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import SavedNotes from './components/SavedNotes';

const Stack = createNativeStackNavigator();

export default function App() {

  const [note, setNote] = useState();
  const [notes, setNotes] = useState([]);
  const [savedNotes, setSavedNotes] = useState([]);

  function handleNote() {
    let newNote = note;
    let newNotes = [newNote, ...notes];
    setNotes(newNotes);
    setNote('');

  }
  useEffect(() => {
    loadNotes();
  }, []);

  const loadNotes = () => {
    AsyncStorage.getItem('storedNotes').then(data => {
      if (data !== null) {
        setNotes(JSON.parse(data));
      }
    }).catch((error) => console.log(error));

    AsyncStorage.getItem('savedNotes').then(data => {
      if (data !== null) {
        setSavedNotes(JSON.parse(data));
      }
    }).catch((error) => console.log(error));
  }
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name='Snote'>
          {props => <Notes {...props}
            notes={notes} setNotes={setNotes}
            note={note} setNote={setNote}
            savedNotes={savedNotes} setSavedNotes={setSavedNotes} />}
        </Stack.Screen>

        <Stack.Screen name='AddNote'>
          {props => <AddNote {...props}
            note={note} setNote={setNote}
            handleNote={handleNote} />}
        </Stack.Screen>

        <Stack.Screen name='SavedNotes'>
          {props => <SavedNotes {...props}
            savedNotes={savedNotes} setSavedNotes={setSavedNotes}
            notes={notes} setNotes={setNotes} />}
        </Stack.Screen>
      </Stack.Navigator>
    </NavigationContainer>


  );
}

