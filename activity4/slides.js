export default [
  {
    id: '1',
    title: 'Why I choose IT course?',
    description: 'If you change your mindset, you have the ability to change to achieve your dreams.',
    image: require('./assets/images/introduction.png'),
  },
  {
    id: '2',
    title: 'Curiosity',
    description: 'I want to learn how they achieve such a amusing application that makes life easier.',
    image: require('./assets/images/mobile.png'),
  },
  {
    id: '3',
    title: 'Dream',
    description: 'I want to make an application that can be significant in everyday life.',
    image: require('./assets/images/thingstosay.png'),
  },
  {
    id: '4',
    title: 'Lucky',
    description: 'It is only a little chance that I can passed the interview for a reason that many students that aiming for these course.',
    image: require('./assets/images/personalfinance.png'),
  },
  {
    id: '5',
    title: 'Knowledge',
    description: 'I want to gain more ideas that will help me to become a professional someday.',
    image: require('./assets/images/powerful.png'),
  },
  {
    id: '6',
    title: 'Fear',
    description: 'I want to face my fear to gain trust, in order to have a self-confidence.',
    image: require('./assets/images/closetab.png'),
  },
  {
    id: '7',
    title: 'Inspired',
    description: 'One of the reason that I choose this course, is to know how the life of one person has change of this course and become a better one.',
    image: require('./assets/images/starlink.png'),
  },
];
